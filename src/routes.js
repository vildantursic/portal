import VueRouter from 'vue-router'
import Home from './pages/Home.vue'
import Forum from './pages/Forum.vue'
import Tread from './pages/Tread.vue'

const routes = [
    { path: '/', component: Home },
    { path: '/forum', component: Forum },
    { path: '/tread/:id', component: Tread }
];

export const router = new VueRouter({
    routes
});